<!DOCTYPE html>
<html>
<head>
	<title>Movie Recommender App</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>
<body>

	<?php require("navbar.php") ?>
	<?php get_content(); ?>
	<?php require("footer.php") ?>

</body>
</html>